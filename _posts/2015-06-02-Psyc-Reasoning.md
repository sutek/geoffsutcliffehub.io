---
layout: post
title: "Psychology of reasoning"
description: ""
category: 
tags: [reasoning, logic]
---
{% include JB/setup %}

### From Cognitive Science 2nd edition - section 4.4.

In reasoning - for example, _modus tollens_ (if A then B) there is an *Inferential transition* from the abstract
to specific instances - in standard terminology, this transition is *domain-general*. It is applicable regardless
of the specifics.

Examples from the Wason selection tests show that humans are not very good at conditional reasoning in the abstract,
for example, the fallacy of affirming the consequent.

> being given B, inferring A --> not valid.
> being given !B, inferring !A --> valid.

However, Griggs and Cox transformed the abstract Wason selection test, and put it into a real-world domain (specifically,
a deontic domain).
Humans then gave much better quality results in problem solving. 

This performance improvement was noticed when the conditional logic in-question was framed as a deontic conditional --
that is, to do with permissions, entitlements, and/or prohibitions.

The inference from this is that humans have a _domain-specific_ competence for reasoning involving deontic conditionals.

### Why is this interesting?

I think this is of interest because of the idea which is furthered in the next section of the text, that humans have
_specialized modules_.

This section of the text is talking about the integration problem, and consideration of how reasoning works -- if it is not
pure abstract reasoning -- is a valuable part of what needs to be in a model of the mind. 

I also want to find out more about deontic logic / reasoning -- and it's applicability to AGI.

This paper may be of interest : [Automated Reasoning in Deontic Logic][1]

[1]: http://arxiv.org/pdf/1411.4823v1.pdf