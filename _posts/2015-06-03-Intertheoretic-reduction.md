---
layout: post
title: "Intertheoretic Reduction"
description: ""
category: 
tags: [philosophy]
---
{% include JB/setup %}

## From Cognitive Science 2nd edition - section 5.1.

### Logical Positivism

Non-fundamental sciences can be reduced to the more fundamental ones, and ultimately, to the most basic science.
This chapter approaches the traditional concept of intertheoretic reduction, and the possibility that it is insufficient
for cognitive science.

