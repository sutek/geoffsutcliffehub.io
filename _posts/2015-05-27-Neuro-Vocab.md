---
layout: post
title: "Neuroscience vocabulary"
description: ""
category: 
tags: [neuroscience]
---
{% include JB/setup %}

### From Cognitive Science 2nd edition - section 3.2.

- Rostral - at the front of the brain.
- Caudal - at the back of the brain.
- Ventral - at the bottom of the brain.
- Dorsal - at the top of the brain.
- Ipsilateral - same side of the brain.
- Contralateral - opposite side of the brain.

This was introduced as part of explaining Ungerleider and Mishkin's two-visual-systems hypothesis: 
Information relevant to recognizing & identifying objects follows a ventral route from the primary visual
cortex (in the occipital lobe) to the temporal lobe (lobes are _regions_ - 4 per hemisphere) - at the bottom. 
Information relevant to locating objects in space follows a dorsal route from the primary visual cortex 
to the posterior parietal lobe (the back part of the top lobe).
Visual information for object identification travels to the bottom, and location information travels to the top.

### Cross-lesion disconnection experiments

Just by observing a specific cognitive problem and a specific damaged area is insufficient to tell
us what that area is for -- the cognitive problem could have been caused by a downstream area which
depends on input from the damaged area.

Making use of the fact that there are two redundant hemispheres (fully redundant?), 
these experiments are performed if the researcher suspects a path - the start node can be removed from
one hemisphere and the end node on the other hemisphere.
The path will proceed across the corpus callosum if there is indeed a pathway. 
If the CC is removed (*transection*), the cognitive problem is experienced.