---
layout: post
title: "Computation Theory - points needed to brush up on"
description: ""
category: 
tags: [mathematics theory]
---
{% include JB/setup %}

Read the Sipser.Introduction.Mathematical Notions and Terminology.Graphs section in the textbook for an overview of the formal description of graphs

My first guess was incorrect, which intuited, just the connectivity.
My second guess was correct, and specified first, the domain in a separate set -- and then the range in another set.
I think this is the principle of the formal description of graphs, but will need to verify this.


Also, there was a question - the answer was referring to:
https://en.wikipedia.org/wiki/Complement_(set_theory)#Relative_complement
