---
layout: page
title: Sutek's AI Blog
tagline: AI, Cognitive Science, and Computer Science notes
full_posts: 2
---
{% include JB/setup %}

<table>
{% for post in site.posts %}
  {% if forloop.index <= page.full_posts %}
  <div class='post'>
    <span class='date'>{{post.date | date_to_string}}</span>
    <h1><a href='{{post.url}}'>{{post.title}}</a></h1>
    <div class='body'>{{post.content}}</div>
    <a href='{{post.url}}#disqus_thread'>View Comments</a>
    <hr>
  </div>
  {% else %}
    {% if forloop.index == page.full_posts %}
    <h3>Older Posts</h3>
    <table class='post-list'>
    {% endif %}
    <tr>
      <th><a href='{{ post.url }}'>{{ post.title }}</a></th>
      <td>{{ post.date | date_to_string }}</td>
      <td><a href='{{post.url}}#disqus_thread'>Comments</a></td>
    </tr>
  {% endif %}
{% endfor %}
</table>
